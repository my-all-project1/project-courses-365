var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesObj = gCoursesDB.courses;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function(){
    onPageLoading();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */ 
//Hàm xử lý sự kiện tải trang
function onPageLoading(){
    console.log(gCoursesObj);
    handlerMostPopular();
    handlerTrending();
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm xử lý lấy được dữ liệu isPopular và đổ vào vùng Popular
function handlerMostPopular(){
    var vPopularObj = gCoursesObj.filter(function(item){
        return item.isPopular;
    })
    console.log("Popular Course Obj : ")
    console.log(vPopularObj);
    loadDataCourseToMostPopular(vPopularObj);
    $("#popularContent >.card-popular:last").remove();
    $("#popularContent >.card-popular:last").remove();

} 

//Hàm xử lý lấy được dữ liệu isTrending và đổ vào vùng Trending 
function handlerTrending(){
    var vTrendingObj = gCoursesObj.filter(function(item){
        return item.isTrending;
    })
    console.log("Trending Course Obj : ")
    console.log(vTrendingObj);
    loadDataCourseToTrending(vTrendingObj);
    $("#trendingContent >.card-trending:last").remove();
    $("#trendingContent >.card-trending:last").remove();
}

//Hàm xử lý load dữ liệu vào vùng loadDataCourseToTrending
function loadDataCourseToMostPopular(paramObj){
    for( var bI = 0 ; bI < paramObj.length ; bI ++){
        var vCardContent = `
        <div class="col-sm-3 card-popular">
              <div class="card">
                <img src="${paramObj[bI].coverImage}" class="card-img-top">
                <div class="card-body">
                  <h6 class="card-title text-primary">${paramObj[bI].courseName}</h6>
                  <div class="card-body-second">
                    <i class="far fa-clock"></i>&nbsp;${paramObj[bI].duration} ${paramObj[bI].level}
                    <p class="card-text">$${paramObj[bI].price} -> <strong>$${paramObj[bI].discountPrice}</strong></p>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="card-footer-left">
                    <img src="${paramObj[bI].teacherPhoto}" class="img-footer">
                    <p class="card-text-footer">${paramObj[bI].teacherName}</p>
                  </div>
                  <div class="card-footer-right">
                    <i class="far fa-bookmark"></i>
                  </div>
                </div>
              </div>
            </div>
        `
        $("#popularContent").append(vCardContent);
    }
}

function loadDataCourseToTrending(paramObj){
    for( var bI = 0 ; bI < paramObj.length ; bI ++){
        var vCardContent = `
        <div class="col-sm-3 card-trending">
              <div class="card">
                <img src="${paramObj[bI].coverImage}" class="card-img-top">
                <div class="card-body">
                  <h6 class="card-title text-primary">${paramObj[bI].courseName}</h6>
                  <div class="card-body-second">
                    <i class="far fa-clock"></i>&nbsp;${paramObj[bI].duration} ${paramObj[bI].level}
                    <p class="card-text">$${paramObj[bI].price} -> <strong>$${paramObj[bI].discountPrice}</strong></p>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="card-footer-left">
                    <img src="${paramObj[bI].teacherPhoto}" class="img-footer">
                    <p class="card-text-footer">${paramObj[bI].teacherName}</p>
                  </div>
                  <div class="card-footer-right">
                    <i class="far fa-bookmark"></i>
                  </div>
                </div>
              </div>
            </div>
        `
        $("#trendingContent").append(vCardContent);
    }
}